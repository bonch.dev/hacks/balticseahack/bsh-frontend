import moment from 'moment'
import API from '../../boot/API'
import { eventRateNames } from './event_rate'
import { specialEventItemLikeNames } from './special_event_item_likes'
import { specialEventItemNames } from './special_event_items'

export const eventNames = {
  EVENT_START: 'EVENT_START',

  EVENTS_LIST_REQUEST: 'EVENTS_LIST_REQUEST',
  EVENTS_LIST_ERROR: 'EVENTS_LIST_ERROR',
  EVENTS_LIST_SUCCESS: 'EVENTS_LIST_SUCCESS',

  CREATE_EVENT_REQUEST: 'CREATE_EVENT_REQUEST',
  CREATE_EVENT_ERROR: 'CREATE_EVENT_ERROR',
  CREATE_EVENT_SUCCESS: 'CREATE_EVENT_SUCCESS',

  UPDATE_EVENT_REQUEST: 'UPDATE_EVENT_REQUEST',
  UPDATE_EVENT_ERROR: 'UPDATE_EVENT_ERROR',
  UPDATE_EVENT_SUCCESS: 'UPDATE_EVENT_SUCCESS'
}
export const eventState = { }

export const eventGetters = {
  eventsAll: state => state.events.filter((event) => {
    return event.is_special === false
  }),
  eventsByUser: (state, getters) => userId => getters.eventsAll.filter((event) => {
    return event.user_id === userId
  }),
  eventsByDate: (state, getters) => (date = moment()) => getters.eventsAll.filter((event) => {
    return date.isBetween(moment(event.from), moment(event.to))
  }),
  events: (state, getters) => getters.eventsAll.filter((event) => {
    let now = moment()
    return now.isBetween(moment(event.from), moment(event.to))
  }),
  eventRates: state => eventId => state.eventRates.filter((rate) => {
    return rate.event_id === eventId
  }),
  eventRateAvg: (state, getters) => eventId => {
    let total = 0
    let eventRates = getters.eventRates(eventId)
    eventRates.forEach((rate) => {
      total += rate.mark
    })
    return total / eventRates.length
  }
}

export const eventActions = {
  [eventNames.EVENT_START]: ({ dispatch }) => {
    dispatch(eventNames.EVENTS_LIST_REQUEST)
    dispatch(eventRateNames.EVENT_RATES_LIST_REQUEST)
    dispatch(specialEventItemNames.EVENT_ITEMS_LIST_REQUEST)
    dispatch(specialEventItemLikeNames.EVENT_ITEM_LIKES_LIST_REQUEST)
  },
  [eventNames.EVENTS_LIST_REQUEST]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      commit(eventNames.EVENTS_LIST_REQUEST)
      API.get('events?_expand=priority&_expand=user&_embed=event_rates')
        .then(resp => {
          commit(eventNames.EVENTS_LIST_SUCCESS, resp.data)
          resolve(resp.data)
        })
        .catch(resp => {
          commit(eventNames.EVENTS_LIST_ERROR)
          reject(resp)
        })
    })
  },
  [eventNames.CREATE_EVENT_REQUEST]: ({ commit, dispatch }, event) => {
    return new Promise((resolve, reject) => {
      commit(eventNames.CREATE_EVENT_REQUEST)
      API.post('events', event)
        .then(resp => {
          commit(eventNames.CREATE_EVENT_SUCCESS, resp.data)
          resolve(resp)
        })
        .catch(resp => {
          commit(eventNames.CREATE_EVENT_ERROR)
          reject(resp)
        })
    })
  },
  [eventNames.UPDATE_EVENT_REQUEST]: ({ commit, dispatch }, event) => {
    return new Promise((resolve, reject) => {
      commit(eventNames.CREATE_EVENT_REQUEST)
      API.patch(`events/${event.id}`, event)
        .then(resp => {
          commit(eventNames.UPDATE_EVENT_SUCCESS, resp.data)
          resolve(resp)
        })
        .catch(resp => {
          commit(eventNames.UPDATE_EVENT_ERROR)
          reject(resp)
        })
    })
  }
}

export const eventMutations = {
  [eventNames.EVENTS_LIST_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [eventNames.EVENTS_LIST_SUCCESS]: (state, events) => {
    state.status = 'success'
    state.events = events
  },
  [eventNames.EVENTS_LIST_ERROR]: (state) => {
    state.status = 'error'
  },

  [eventNames.CREATE_EVENT_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [eventNames.CREATE_EVENT_SUCCESS]: (state, event) => {
    state.status = 'success'
    state.events.push(event)
  },
  [eventNames.CREATE_EVENT_ERROR]: (state) => {
    state.status = 'error'
  },

  [eventNames.UPDATE_EVENT_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [eventNames.UPDATE_EVENT_SUCCESS]: (state, event) => {
    state.status = 'success'
    let index = state.events.findIndex((elem) => elem.id === event.id)
    this.set(state.events, index, event)
  },
  [eventNames.UPDATE_EVENT_ERROR]: (state) => {
    state.status = 'error'
  }
}
