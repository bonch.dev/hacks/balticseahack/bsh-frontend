import API from '../../boot/API'

export const messageNames = {
  MESSAGES_LIST_REQUEST: 'MESSAGES_LIST_REQUEST',
  MESSAGES_LIST_SUCCESS: 'MESSAGES_LIST_SUCCESS',
  MESSAGES_LIST_ERROR: 'MESSAGES_LIST_ERROR',

  CREATE_MESSAGE_REQUEST: 'CREATE_MESSAGE_REQUEST',
  CREATE_MESSAGE_SUCCESS: 'CREATE_MESSAGE_SUCCESS',
  CREATE_MESSAGE_ERROR: 'CREATE_MESSAGE_ERROR'
}

export const messageState = {
  messages: []
}

export const messageGetters = {
  messages: state => state.messages,
  messagesByEvent: state => eventId => state.messages.filter((message) => {
    return message.event_id === eventId
  })
}

export const messageActions = {
  [messageNames.MESSAGES_LIST_REQUEST]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      commit(messageNames.MESSAGES_LIST_REQUEST)
      API.get('messages')
        .then(resp => {
          commit(messageNames.MESSAGES_LIST_SUCCESS, resp.data)
          resolve(resp.data)
        })
        .catch(resp => {
          commit(messageNames.MESSAGES_LIST_ERROR)
          reject(resp)
        })
    })
  },
  [messageNames.CREATE_MESSAGE_REQUEST]: ({ commit, dispatch }, event) => {
    return new Promise((resolve, reject) => {
      commit(messageNames.CREATE_MESSAGE_REQUEST)
      API.post('messages', event)
        .then(resp => {
          commit(messageNames.CREATE_MESSAGE_SUCCESS, resp.data)
          resolve(resp)
        })
        .catch(resp => {
          commit(messageNames.CREATE_MESSAGE_ERROR)
          reject(resp)
        })
    })
  }
}

export const messageMutations = {
  [messageNames.MESSAGES_LIST_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [messageNames.MESSAGES_LIST_SUCCESS]: (state, messages) => {
    state.status = 'success'
    state.messages = messages
  },
  [messageNames.MESSAGES_LIST_ERROR]: (state) => {
    state.status = 'error'
  },

  [messageNames.CREATE_MESSAGE_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [messageNames.CREATE_MESSAGE_REQUEST]: (state, message) => {
    state.status = 'success'
    state.messages.push(message)
  },
  [messageNames.CREATE_MESSAGE_REQUEST]: (state) => {
    state.status = 'error'
  }
}
