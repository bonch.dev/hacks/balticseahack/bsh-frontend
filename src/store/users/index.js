import API from '../../boot/API'

export const userNames = {
  START: 'START',

  SOCIAL_LIST_REQUEST: 'SOCIAL_LIST_REQUEST',
  SOCIAL_LIST_ERROR: 'SOCIAL_LIST_ERROR',
  SOCIAL_LIST_SUCCESS: 'SOCIAL_LIST_SUCCESS',

  MODIFY_COIN_REQUEST: 'MODIFY_COIN_REQUEST',
  MODIFY_COIN_ERROR: 'MODIFY_COIN_ERROR',
  MODIFY_COIN_SUCCESS: 'MODIFY_COIN_SUCCESS',

  PROFILE_REQUEST: 'PROFILE_REQUEST',
  PROFILE_ERROR: 'PROFILE_ERROR',
  PROFILE_SUCCESS: 'PROFILE_SUCCESS',

  USERS_LIST_REQUEST: 'USERS_LIST_REQUEST',
  USERS_LIST_ERROR: 'USERS_LIST_ERROR',
  USERS_LIST_SUCCESS: 'USERS_LIST_SUCCESS',

  CREATE_USER_REQUEST: 'CREATE_USER_REQUEST',
  CREATE_USER_ERROR: 'CREATE_USER_ERROR',
  CREATE_USER_SUCCESS: 'CREATE_USER_SUCCESS',

  UPDATE_USER_REQUEST: 'UPDATE_USER_REQUEST',
  UPDATE_USER_ERROR: 'UPDATE_USER_ERROR',
  UPDATE_USER_SUCCESS: 'UPDATE_USER_SUCCESS'
}

const state = {
  status: '',
  users: [],
  socials: [],
  profile: {}
}

const getters = {
  users: state => state.users,
  profile: state => state.profile,
  socials: state => state.socials,
  userById: state => id => state.users.find(user => user.id === id)
}

const actions = {
  [userNames.START]: ({ dispatch }) => {
    dispatch(userNames.SOCIAL_LIST_REQUEST)
    dispatch(userNames.PROFILE_REQUEST)
    dispatch(userNames.USERS_LIST_REQUEST)
  },
  [userNames.SOCIAL_LIST_REQUEST]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      commit(userNames.SOCIAL_LIST_REQUEST)
      API.get('socials')
        .then(resp => {
          commit(userNames.SOCIAL_LIST_SUCCESS, resp.data)
          resolve(resp.data)
        })
        .catch(resp => {
          commit(userNames.SOCIAL_LIST_ERROR)
          reject(resp)
        })
    })
  },
  [userNames.PROFILE_REQUEST]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      commit(userNames.PROFILE_REQUEST)
      API.get('profile/?_embed=events&_embed=event_items&_expand=user_type&_embed=user_social&_embed=event_rates&_embed=item_user')
        .then(resp => {
          commit(userNames.PROFILE_SUCCESS, resp.data)
          resolve(resp.data)
        })
        .catch(resp => {
          commit(userNames.PROFILE_ERROR)
          reject(resp)
        })
    })
  },
  [userNames.USERS_LIST_REQUEST]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      commit(userNames.USERS_LIST_REQUEST)
      API.get('users?_embed=events&_embed=event_items&_expand=user_type&_embed=user_social&_embed=event_rates')
        .then(resp => {
          commit(userNames.USERS_LIST_SUCCESS, resp.data)
          resolve(resp.data)
        })
        .catch(resp => {
          commit(userNames.USERS_LIST_ERROR)
          reject(resp)
        })
    })
  },
  [userNames.MODIFY_COIN_REQUEST]: ({ commit, dispatch, getters }, { amount = 0, userId = null }) => {
    return new Promise((resolve, reject) => {
      let profile = null
      if (userId === null) {
        profile = state.profile
        userId = profile.id
      } else {
        profile = getters.userById(userId)
      }

      commit(userNames.MODIFY_COIN_REQUEST)
      if (amount < 0) {
        let newBalance = profile.balance + amount
        if (newBalance < 0) {
          return reject('Not enough coins in a balance')
        }
        API.patch(`users/${userId}`, {
          balance: newBalance
        }).catch(resp => {
          commit(userNames.MODIFY_COIN_ERROR)
          reject(resp)
        })
      } else {
        API.patch(`users/${userId}`, {
          balance: profile.balance + amount
        }).catch(resp => {
          commit(userNames.MODIFY_COIN_ERROR)
          reject(resp)
        })
      }
      dispatch(userNames.USERS_LIST_REQUEST)
      dispatch(userNames.PROFILE_REQUEST)
      commit(userNames.MODIFY_COIN_SUCCESS)
      resolve()
    })
  },
  [userNames.CREATE_USER_REQUEST]: ({ commit, dispatch }, user) => {
    return new Promise((resolve, reject) => {
      commit(userNames.CREATE_USER_REQUEST)
      API.post('users', user)
        .then(resp => {
          commit(userNames.CREATE_USER_SUCCESS, resp.data)
          resolve(resp)
        })
        .catch(resp => {
          commit(userNames.CREATE_USER_ERROR)
          reject(resp)
        })
    })
  },
  [userNames.UPDATE_USER_REQUEST]: ({ commit }, user) => {
    return new Promise((resolve, reject) => {
      commit(userNames.CREATE_USER_REQUEST)
      API.patch(`users/${user.id}`, user)
        .then(resp => {
          commit(userNames.UPDATE_USER_SUCCESS, resp.data)
          resolve(resp)
        })
        .catch(resp => {
          commit(userNames.UPDATE_USER_ERROR)
          reject(resp)
        })
    })
  }
}

const mutations = {
  [userNames.SOCIAL_LIST_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [userNames.SOCIAL_LIST_SUCCESS]: (state, socials) => {
    state.status = 'success'
    state.socials = socials
  },
  [userNames.SOCIAL_LIST_ERROR]: (state) => {
    state.status = 'error'
  },

  [userNames.USERS_LIST_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [userNames.USERS_LIST_SUCCESS]: (state, users) => {
    state.status = 'success'
    state.users = users
  },
  [userNames.USERS_LIST_ERROR]: (state) => {
    state.status = 'error'
  },

  [userNames.CREATE_USER_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [userNames.CREATE_USER_SUCCESS]: (state, user) => {
    state.status = 'success'
    state.users.push(user)
  },
  [userNames.CREATE_USER_ERROR]: (state) => {
    state.status = 'error'
  },

  [userNames.UPDATE_USER_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [userNames.UPDATE_USER_SUCCESS]: (state, user) => {
    state.status = 'success'
    let index = state.users.findIndex((elem) => elem.id === user.id)
    this.set(state.users, index, user)
  },
  [userNames.UPDATE_USER_ERROR]: (state) => {
    state.status = 'error'
  },

  [userNames.PROFILE_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [userNames.PROFILE_SUCCESS]: (state, profile) => {
    state.status = 'success'
    state.profile = profile
  },
  [userNames.PROFILE_ERROR]: (state) => {
    state.status = 'error'
  },

  [userNames.MODIFY_COIN_ERROR]: (state) => {
    state.status = 'error'
  },
  [userNames.MODIFY_COIN_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [userNames.MODIFY_COIN_SUCCESS]: (state) => {
    state.status = 'loading'
  }
}

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
}
